#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

int main (int argc, char *argv[]) 
{
    int show_permission = 0;
    int opt;
    FILE *f;
    char c;
    while ((opt = getopt(argc, argv, "p")) != -1)
    {

        switch (opt)
        {
        case 'p':
            show_permission = 1;
            break;
        default:
            fprintf(stderr, "Usage: %s [-p] [directory]\n", argv[0]);
            return 1;
        }
    }

    if (argc == 2)
    {
        while ((c = fgetc(stdin)) != EOF)
        {
            fputc(c, stdout);
        }
    }
    else
    {
        // on ouvre le fichier avec la fonction fopen
        f = fopen("tp-1/samples/input.txt", "r");
        if (f == NULL)
        {
            perror("Fichier n'existe pas \n");
            return 1;
        }

        while ((c = fgetc(f)) != EOF)
        {
            fputc(c, stdout);
        }

        fclose(f);
    }

    // pour l'exercice 3 on crée une liste avec les char de la liste de mots, et on indique les séparateurs comme vu dans la doc

    
    // on ouvre le fichier input.txt pour l'intégrer à char
    char str[] = fopen("tp-1/samples/input.txt", "r");
    // on indique les séparateurs, comme indiqué dans la doc
    const char *separators = " ,!";

    char *strToken = strtok(str, separators);
    int mots = 0;
    char supprime;
    char remplace;

    // pour l'exercice 4, on demande quel mot est à remplacer, puis par quoi on le remplace
    printf("saisissez un mot à remplacer \n");
    scanf("%c", &supprime);
    printf("à remplacer par :\n");
    scanf("%c", &remplace);


    while (strToken != NULL)
    {
        // on incrémente chaque mot de la boucle dans la variable "mots" 
        mots = mots + 1;
        // dans la boucle qui sort chaque mot de la liste, on teste si le mot correspond à celui que l'on souhaite remplacer et on le remplace si c'est le cas
        if ( strToken == supprime)
        {
            strToken = remplace;
        }
        printf("%s\n", strToken);
       
        // afficher chaque mot n'est pas demandé, mais c'est plus parlant
        strToken = strtok(NULL, separators);
        
    }
    // on affiche le nombre total de mots
    printf("nombre de mots : %i \n", mots);


    }



